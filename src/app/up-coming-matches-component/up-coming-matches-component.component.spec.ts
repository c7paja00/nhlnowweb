import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpComingMatchesComponentComponent } from './up-coming-matches-component.component';

describe('UpComingMatchesComponentComponent', () => {
  let component: UpComingMatchesComponentComponent;
  let fixture: ComponentFixture<UpComingMatchesComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpComingMatchesComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpComingMatchesComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
