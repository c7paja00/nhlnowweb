import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
//import { Match } from '../match';
import Match from '../Match';
import { MatchArray } from '../matchArray';

@Component({
  selector: 'app-up-coming-matches-component',
  templateUrl: './up-coming-matches-component.component.html',
  styleUrls: ['./up-coming-matches-component.component.css']
})

export class UpComingMatchesComponentComponent implements OnInit {

  constructor(private apiService: ApiService) {}

  ngOnInit(): void {
    this.getUpComingMatches();
  }

  datesArray: any = []; //array
  date;                 //obj
  gamesArray: any = []; //array
  teams;                //obj

  awayAway;             //obj
  teamAway;             //obj
  nameAway;             //key
  idAway;               //key

  homeHome;             //obj
  teamHome;             //obj
  nameHome;             //key
  idHome;               //key

  matchArray: MatchArray = [];
  i: number = 0;

  getUpComingMatches() {
    this.apiService.getUpComingMatches().subscribe((data) => {
    this.datesArray = data['dates'];
    //console.log(data);
    //console.log(this.datesArray);

    for (const c of (this.datesArray as any)) {
      this.date = c['date']
      //console.log(this.date);

      this.gamesArray = c['games']
      //console.log(this.gamesArray);
      for (const d of (this.gamesArray as any)) {
      
        this.teams = d['teams']
        //console.log(this.teams);

        this.awayAway = this.teams['away']
        this.homeHome = this.teams['home']

        this.teamAway = this.awayAway['team']
        this.teamHome = this.homeHome['team']

        this.idAway = this.teamAway['id']
        this.idHome = this.teamHome['id']

        this.nameAway = this.teamAway['name']
        this.nameHome = this.teamHome['name']
        //console.log('Date: ' + this.date + ' ' + this.nameAway + ' VS ' + this.nameHome);
        
        var match = new Match(this.date, this.nameHome, this.idHome, this.nameAway, this.idAway);
        //console.log(match.MatchDate + d);
        this.matchArray[this.i] = match;
        this.i++;
      }
    }
    });
  }
}
