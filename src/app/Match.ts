export default class Match {
    MatchDate: string;
    MatchHomeTeam: string;
    MatchHomeId: string;
    MatchAwayTeam: string;
    MatchAwayId: string;

    constructor(MatchDate, MatchHomeTeam, MatchHomeId, MatchAwayTeam, MatchAwayId){
      this.MatchDate = MatchDate;
      this.MatchHomeTeam = MatchHomeTeam;
      this.MatchHomeId = MatchHomeId;
      this.MatchAwayTeam = MatchAwayTeam;
      this.MatchAwayId = MatchAwayId;
  }

  }
