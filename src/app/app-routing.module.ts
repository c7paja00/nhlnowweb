import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamStatsComponent} from './team-stats/team-stats.component';
import { TeamPlayerStatsComponent } from './team-player-stats/team-player-stats.component';


const routes: Routes = [
  { path: '', component : TeamPlayerStatsComponent},
  { path: 'stats', component : TeamStatsComponent}

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
