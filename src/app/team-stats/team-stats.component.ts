import { Component, OnInit, ViewEncapsulation, SystemJsNgModuleLoader, Input } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-team-stats',
  templateUrl: './team-stats.component.html',
  styleUrls: ['./team-stats.component.css'],
  encapsulation: ViewEncapsulation.None

})

export class TeamStatsComponent implements OnInit {
    league: any = [];
    leagueStats;
    teamRecordsArray: any = [];
    divisionStatsArray: any = [];
    teamStats;
    teamResult;
    metroStats;
    atlanticStats;
    centralStats;
    pacificStats;

  constructor(private apiService: ApiService, ) {
  }

  ngOnInit(): void {
    this.getTeamStats();
  }

  getTeamStats() {
  this.apiService.getTeamStats().subscribe((data) => {
    this.teamRecordsArray = data['records'];

    console.log(data);
    console.log(this.teamRecordsArray);

    for (const d of (this.teamRecordsArray as any)) {
      this.divisionStatsArray.push({
        divisionRecords: d.teamRecords
      });
    }
    console.log(this.divisionStatsArray);

    this.metroStats = this.divisionStatsArray[0];
    this.atlanticStats = this.divisionStatsArray[1];
    this.centralStats = this.divisionStatsArray[2];
    this.pacificStats = this.divisionStatsArray[3];

    console.log(this.metroStats);
    console.log(this.atlanticStats);
    console.log(this.centralStats);
    console.log(this.pacificStats);
  });
  }
}


