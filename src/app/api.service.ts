import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { TeamStatistics } from './teamstatistics';
import { Observable } from 'rxjs';
import { Visitors } from './visitors';

const teamStatsUrl = 'https://statsapi.web.nhl.com/api/v1/standings';
//const upComingMatchesUrl = 'https://statsapi.web.nhl.com/api/v1/schedule?startDate=2020-02-29&endDate=2020-03-1'
const localUrl = 'assets/data/nhlRecords.json';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) {
  }

  public calculateDaysForURL() : string {
    let date: Date = new Date();
    var day: string = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate();
    console.log(day);

    var i: number = date.getDate()+2;
    date.setDate(i);
    console.log(i);
    
    var dayAfterTwo: string = date.getFullYear() + '-' + (date.getMonth()+1) + '-' + date.getDate();

    console.log(day + ' ' + dayAfterTwo);
    console.log(date);

    const URL = 'https://statsapi.web.nhl.com/api/v1/schedule?startDate=' + day + '&endDate=' + dayAfterTwo
    //upComingMatchesUrl2 = URL;
    return URL;
  }

  public getTeamStats() {
    return this.http.get(teamStatsUrl);
   }

   public getUpComingMatches() {
     return this.http.get(this.calculateDaysForURL());
   }
   
   getTeamRoster(teamId: number){
    const urlString = 'https://statsapi.web.nhl.com/api/v1/teams/' + teamId + '/?expand=team.roster';
    return this.http.get(urlString);
  }

  private visitorsUrl = '';

  // get("/api/contacts")
  getVisitors(): Promise<void | Visitors[]> {
    return this.http.get(this.visitorsUrl)
               .toPromise()
               .then(response => response as Visitors[])
               .catch(this.handleError);
  }

  // post("/api/contacts")
  createVisitor(newVisitor: Visitors): Promise<void | Visitors> {
    return this.http.post(this.visitorsUrl, newVisitor)
               .toPromise()
               .then(response => response as Visitors)
               .catch(this.handleError);
  }

  private handleError (error: any) {
    let errMsg = (error.message) ? error.message :
    error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
  }
}

