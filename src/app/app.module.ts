import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UpComingMatchesComponentComponent } from './up-coming-matches-component/up-coming-matches-component.component';
import { TeamPlayerStatsComponent } from './team-player-stats/team-player-stats.component';
import { TeamStatsComponent } from './team-stats/team-stats.component';
import { HttpClientModule } from '@angular/common/http';
import { ApiService} from './api.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { VisitorsComponentComponent } from './visitors-component/visitors-component.component';



@NgModule({
  declarations: [
    AppComponent,
    UpComingMatchesComponentComponent,
    TeamPlayerStatsComponent,
    TeamStatsComponent,
    VisitorsComponentComponent,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [ApiService],
  bootstrap: [AppComponent]
})
export class AppModule { }
