import { Component, OnInit } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
//import { HttpService } from '../http.service';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-team-player-stats',
  templateUrl: './team-player-stats.component.html',
  styleUrls: ['./team-player-stats.component.css']
})
export class TeamPlayerStatsComponent implements OnInit {


  teamLogos: ImageData;
  teamId: number;
  teamRoster: any = [];
  event: Event;

  constructor(private apiHttp: ApiService) { }

  ngOnInit(): void {

  }


  getTeamRoster(event) {
    console.log('Team logo clicked!');
    //console.log(event.toElement.id);
    //const teamId = event.toElement.id;
    console.log(event.target.id);
    const teamId = event.target.id;


    console.log(teamId);

    this.apiHttp.getTeamRoster(teamId).subscribe(data => {
      const teams = data['teams'];
      //console.log(teams);

      const firstObj = teams['0'];
      //console.log(firstObj);

      const rosterObj = firstObj['roster'];
      console.log(rosterObj);

      const rosterArray = rosterObj.roster;
      this.teamRoster = rosterArray;

      console.log(this.teamRoster);
    });

  }

}
