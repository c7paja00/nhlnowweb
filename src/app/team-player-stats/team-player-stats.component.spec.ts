import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamPlayerStatsComponent } from './team-player-stats.component';

describe('TeamPlayerStatsComponent', () => {
  let component: TeamPlayerStatsComponent;
  let fixture: ComponentFixture<TeamPlayerStatsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeamPlayerStatsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamPlayerStatsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });


});
