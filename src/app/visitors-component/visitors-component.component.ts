import { Component, OnInit } from '@angular/core';
import { Visitors } from '../visitors';
import { ApiService } from '../api.service'

@Component({
  selector: 'app-visitors-component',
  templateUrl: './visitors-component.component.html',
  styleUrls: ['./visitors-component.component.css'],
  providers: [ApiService]
})
export class VisitorsComponentComponent implements OnInit {

  visitors: Visitors[]
  selectedVisitor: Visitors
  count: number = 1
  countString: string = "" + this.count
  
  constructor(private apiService: ApiService) { }

  ngOnInit(): void {
    /*this.apiService.getVisitors().then((visitors: Visitors[]) => {
      this.visitors = visitors.map((visitors) => {
        return visitors;
      })
    })*/
    this.createNewVisitor()
    console.log(this.count)
  }

  private getIndexOfVisitors = (visitorsId: String) => {
    return this.visitors.findIndex((visitors) => {
      return visitors._id === visitorsId;
    })
  }

  countVisitors() {
    return this.countString;
  }

  createNewVisitor() {
    var visitors: Visitors = {
      count: this.countString
    }
    this.count++
  }

}
