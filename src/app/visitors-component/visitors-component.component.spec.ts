import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisitorsComponentComponent } from './visitors-component.component';

describe('VisitorsComponentComponent', () => {
  let component: VisitorsComponentComponent;
  let fixture: ComponentFixture<VisitorsComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisitorsComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisitorsComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
