import Match from './Match';

export interface MatchArray {
    [position: number]: Match;
}
