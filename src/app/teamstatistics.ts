export interface TeamStatistics {
  id: string;
  name: string;
  wins: string;
  losses: string;
  otlosses: string;
  points: string;
}
