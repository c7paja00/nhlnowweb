const express = require('express')
const app = express();

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/src/index.html')
});

app.listen(process.env.PORT || 8000, function(){
  console.log('Example app listening on port 8000!')
  var port = server.address().port;
  console.log("App now running on port", port);
});
