var express = require("express");
var bodyParser = require("body-parser");
var mongodb = require("mongodb");
var ObjectID = mongodb.ObjectID;

var app = express();
app.use(bodyParser.json());

var distDir = __dirname + "/dist/";
app.use(express.static(distDir));

// Create a database variable outside of the database connection callback to reuse the connection pool in your app.
var db;
var collection;
 
const MongoClient = require('mongodb').MongoClient;
// replace the uri string with your connection string.
 
const uri = "mongodb+srv://pekkapekka:PekkaKakka@cluster0-sbl73.mongodb.net/test?retryWrites=true&w=majority"
 
MongoClient.connect(uri, function(err, client) {
    
    if(err) {
        console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
        process.exit(1);
    }
  
    console.log('Connected...');
    collection = client.db("test_db").collection("visitors");
 
    var server = app.listen(process.env.PORT || 8081, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
  });
});

// VISITORS API ROUTES BELOW

// Generic error handler used by all endpoints.
function handleError(res, reason, message, code) {
    console.log("ERROR: " + reason);
    res.status(code || 500).json({"error": message});
}

app.get("/visitors", function(req, res) {
    collection.find({}).toArray(function(err, docs) {
      if (err) {
        handleError(res, err.message, "Failed to get visitors.");
      } else {
        res.status(200).json(docs);  
      }
    });
});

app.post("/visitors", function(req, res) {
    var newVisitor = req.body;
    newVisitor.createDate = new Date();
   
    if (!(req.body.firstName || req.body.lastName)) {
        handleError(res, "Invalid user input", "Must provide a first or last name.", 400);
    }
   
    collection.insertOne(newVisitor, function(err, doc) {
        if (err) {
            handleError(res, err.message, "Failed to create new visitor.");
        } else {
            res.status(201).json(doc.ops[0]);
        }
    });
});  
 

// Initialize the app.
/*
var server = app.listen(process.env.PORT || 8080, function () {
    var port = server.address().port;
    console.log("App now running on port", port);
});*/
